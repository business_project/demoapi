﻿using Entidades.Entidad.Maestro;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;
using Dapper.FluentColumnMapping;
using Datos.Helper;
using Entidades.Dto;

namespace Datos.Maestro
{
    public class AdContacto
    {
        public List<ContactoDto> Get(FiltroContactoDto filtro)
        {
            var mappings = new ColumnMappingCollection();
            mappings.RegisterType<ContactoDto>()
                .MapProperty(x => x.Correo).ToColumn("Email");
            mappings.RegisterWithDapper();

            List<ContactoDto> lista;

            try
            {
                string query = "select top 20 * from Contacto where Nombre like '%'+@Nombre+'%'";
                using (var cn = HelperClass.ObtenerConeccion())
                {
                    if (cn.State == ConnectionState.Closed)
                    {
                        cn.Open();
                    }

                    lista = cn.Query<ContactoDto>(query, new
                    {
                        Nombre = filtro.Buscar
                    }).ToList();

                }

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return lista;
        }

        public Contacto Get(long id)
        {
            Contacto result;
            try
            {
                string query = "select * from Contacto where IdContacto = @IdContacto";
                using (var cn = HelperClass.ObtenerConeccion())
                {
                    if (cn.State == ConnectionState.Closed)
                    {
                        cn.Open();
                    }

                    result = cn.QuerySingleOrDefault<Contacto>(query, new
                    {
                        IdContacto = id
                    });

                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return result;
        }

        public int Create(Contacto entity)
        {
            try
            {
                string query =
                    "INSERT INTO Contacto(IdContacto, Nombre, Email, Telefono) VALUES((SELECT MAX(IdContacto) + 1 FROM Contacto), @Nombre, @Email, @Telefono)";
                using (var cn = HelperClass.ObtenerConeccion())
                {
                    if (cn.State == ConnectionState.Closed)
                    {
                        cn.Open();
                    }

                    return cn.Execute(query, new
                    {
                        entity.Nombre,
                        entity.Email,
                        entity.Telefono
                        //Nombre = entity.Nombre,
                        //Email = entity.Email,
                        //Telefono = entity.Telefono
                    });

                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public int Update(Contacto entity)
        {
            try
            {
                string query =
                    "UPDATE Contacto SET Nombre = @Nombre, Email = @Email, Telefono = @Telefono WHERE IdContacto = @IdContacto";
                using (var cn = HelperClass.ObtenerConeccion())
                {
                    if (cn.State == ConnectionState.Closed)
                    {
                        cn.Open();
                    }

                    return cn.Execute(query, new
                    {
                        entity.IdContacto,
                        entity.Nombre,
                        entity.Email,
                        entity.Telefono
                        //IdContacto = entity.IdContacto,
                        //Nombre = entity.Nombre,
                        //Email = entity.Email,
                        //Telefono = entity.Telefono
                    });

                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

        public int Remove(long id)
        {
            try
            {
                string query =
                    "DELETE FROM Contacto WHERE IdContacto = @IdContacto";
                using (var cn = HelperClass.ObtenerConeccion())
                {
                    if (cn.State == ConnectionState.Closed)
                    {
                        cn.Open();
                    }

                    return cn.Execute(query, new
                    {
                        IdContacto = id
                    });

                }
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
        }

    }
}
