﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Entidades.Dto;
using Entidades.Entidad.Maestro;
using Negocio.Maestro;

namespace Api.Controllers
{
    [RoutePrefix("api/contacto")]
    public class ContactoController : ApiController
    {
        private readonly LnContacto _lnContacto = new LnContacto();

        // GET: api/contacto
        [HttpPost]
        [AcceptVerbs("POST")]
        [Route("Get")]
        //[ValidationActionFilter]
        public IHttpActionResult Get([FromBody] FiltroContactoDto filtro)
        {
            var result = _lnContacto.Get(filtro);
            return Ok(result);
        }

        // GET: api/contacto/5
        [HttpGet]
        [AcceptVerbs("GET")]
        public IHttpActionResult Get(long id)
        {
            var result = _lnContacto.Get(id);
            return Ok(result);
        }

        // POST: api/contacto
        [HttpPost]
        [AcceptVerbs("POST")]
        //[ValidationActionFilter]
        public IHttpActionResult Post([FromBody]Contacto contacto)
        {
            var result = _lnContacto.Create(contacto);
            return Ok(result);
        }

        // PUT: api/contacto/5
        [HttpPut]
        [AcceptVerbs("PUT")]
        //[ValidationActionFilter]
        public IHttpActionResult Put(long id, [FromBody]Contacto contacto)
        {
            if (id != contacto.IdContacto)
                return Content(HttpStatusCode.BadRequest, new HttpError("Error en la petición."));
            var result = _lnContacto.Update(contacto);
            return Ok(result);
        }

        // DELETE: api/contacto/5
        [HttpDelete]
        [AcceptVerbs("DELETE")]
        public IHttpActionResult Delete(long id)
        {
            if (id == 0)
                return Content(HttpStatusCode.BadRequest, new HttpError("Error en la petición."));
            var result = _lnContacto.Remove(id);
            return Ok(result);
        }
    }
}
