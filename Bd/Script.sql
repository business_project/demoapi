/*    ==Scripting Parameters==
	User: Frank
    Source Server Version : SQL Server 2016 (13.0.4001)
    Source Database Engine Edition : Microsoft SQL Server Standard Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2008 R2
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/
CREATE DATABASE [Demo]
GO

USE [Demo]
GO
/****** Object:  Table [dbo].[Contacto]    Script Date: 19/11/2017 23:34:12 ******/
DROP TABLE [dbo].[Contacto]
GO
/****** Object:  Table [dbo].[Contacto]    Script Date: 19/11/2017 23:34:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contacto](
	[IdContacto] [int] NOT NULL,
	[Nombre] [varchar](250) NULL,
	[Email] [varchar](250) NULL,
	[Telefono] [varchar](50) NULL
) ON [PRIMARY]
GO
INSERT [dbo].[Contacto] ([IdContacto], [Nombre], [Email], [Telefono]) VALUES (1, N'nom1', N'ema1', N'tel1')
GO
INSERT [dbo].[Contacto] ([IdContacto], [Nombre], [Email], [Telefono]) VALUES (2, N'nom2', N'ema2', N'tel2')
GO
INSERT [dbo].[Contacto] ([IdContacto], [Nombre], [Email], [Telefono]) VALUES (3, N'nom3', N'ema3', N'tel3')
GO
