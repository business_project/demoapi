﻿using System.ComponentModel.DataAnnotations;

namespace Entidades.Entidad.Maestro
{
    public class Contacto
    {
        public int IdContacto { get; set; }
        [Required]
        [MaxLength(150)]
        public string Nombre { get; set; }
        [Required]
        [MaxLength(150)]
        public string Email { get; set; }
        [Required]
        [MaxLength(50)]
        public string Telefono { get; set; }
    }
}
