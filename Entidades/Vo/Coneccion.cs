﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Vo
{
    public class Coneccion
    {
        public static string Cnx()
        {
            return ConfigurationManager.ConnectionStrings["cn"].ConnectionString;
        }
    }
}
