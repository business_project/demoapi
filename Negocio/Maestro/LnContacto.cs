﻿using System.Collections.Generic;
using Datos.Maestro;
using Entidades.Dto;
using Entidades.Entidad.Maestro;

namespace Negocio.Maestro
{
    public class LnContacto
    {
        private readonly AdContacto _adContacto = new AdContacto();
        public List<ContactoDto> Get(FiltroContactoDto filtro)
        {
            return _adContacto.Get(filtro);
        }

        public Contacto Get(long id)
        {
            return _adContacto.Get(id);
        }

        public int Create(Contacto entity)
        {
            return _adContacto.Create(entity);
        }

        public int Update(Contacto entity)
        {
            return _adContacto.Update(entity);
        }

        public int Remove(long id)
        {
            return _adContacto.Remove(id);
        }

    }
}
